from django.conf.urls import url

from . import views

urlpatterns = [
      url(r'^login', views.login, name='login'),
      url(r'^signup', views.register, name='register'),
      url(r'^welcome', views.welcome, name='welcome'),
#      url(r'^otpgenrate', views.otpgenrate, name='otpgenrate'),      
      url(r'^otp', views.forgot, name='forgot'),
      url(r'^resetpassword', views.reset, name='reset'),

      


]

